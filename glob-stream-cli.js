#!/usr/bin/env node

const path				= require("path");

const GlobStream		= require("glob-stream");

//==============================================================================

(async () => {
	const patterns		= process.argv.slice(2);

	await Promise.all(patterns.map((pattern) => {
		return new Promise((resolve) => {
			const globStream		=
				GlobStream(pattern, {
					allowEmpty:			true
				,	dot:				true
				});
			globStream.on("data", (entry) => {
				//console.log(entry);
				const cwd		= path==path.posix
									? entry.cwd
									: entry.cwd.split(path.sep).join(path.posix.sep);
				let entryPath		= 
					entry.base.substring(0, cwd.length) == cwd
						? path.relative(entry.cwd, entry.path)
						: entry.path;
				console.info(entryPath);
			});
			globStream.on("end", () => {
				resolve();
			});
		});
	}));
})().catch((err) => {
	console.error(err);
	process.exit(1);
});
